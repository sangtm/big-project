<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 20 Aug 2017 03:28:15 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class User
 * 
 * @property int $UserID
 * @property int $FacebookID
 * @property string $UserName
 * @property string $Email
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class User extends Eloquent
{
	protected $table = 'User';
	protected $primaryKey = 'UserID';

	protected $casts = [
		'FacebookID' => 'int'
	];

	protected $hidden = [
		'remember_token'
	];

	protected $fillable = [
		'FacebookID',
		'UserName',
		'Email',
		'remember_token'
	];
}
