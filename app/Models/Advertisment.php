<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 20 Aug 2017 03:28:15 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Advertisment
 * 
 * @property int $AdvertisID
 * @property int $Number_of_adv
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $products
 *
 * @package App\Models
 */
class Advertisment extends Eloquent
{
	protected $table = 'Advertisment';
	protected $primaryKey = 'AdvertisID';
	public $incrementing = false;

	protected $casts = [
		'AdvertisID' => 'int',
		'Number_of_adv' => 'int'
	];

	protected $fillable = [
		'Number_of_adv'
	];

	public function products()
	{
		return $this->hasMany(\App\Models\Product::class, 'AdvertisID');
	}
}
