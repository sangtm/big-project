<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 20 Aug 2017 03:28:15 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Purchase
 * 
 * @property int $id
 * @property int $SupplierID
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Supplier $supplier
 *
 * @package App\Models
 */
class Purchase extends Eloquent
{
	protected $table = 'Purchase';

	protected $casts = [
		'SupplierID' => 'int'
	];

	protected $fillable = [
		'SupplierID',
		'status'
	];

	public function supplier()
	{
		return $this->belongsTo(\App\Models\Supplier::class, 'SupplierID');
	}
}
