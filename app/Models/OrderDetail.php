<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 20 Aug 2017 03:28:15 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderDetail
 * 
 * @property int $OrderDetailID
 * @property int $ProductID
 * @property int $OrderID
 * @property int $Price
 * @property int $Discount
 * @property int $Total_Price
 * @property float $Fee
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Product $product
 * @property \App\Models\Order $order
 *
 * @package App\Models
 */
class OrderDetail extends Eloquent
{
	protected $table = 'OrderDetail';
	protected $primaryKey = 'OrderDetailID';

	protected $casts = [
		'ProductID' => 'int',
		'OrderID' => 'int',
		'Price' => 'int',
		'Discount' => 'int',
		'Total_Price' => 'int',
		'Fee' => 'float'
	];

	protected $fillable = [
		'ProductID',
		'OrderID',
		'Price',
		'Discount',
		'Total_Price',
		'Fee'
	];

	public function product()
	{
		return $this->belongsTo(\App\Models\Product::class, 'ProductID');
	}

	public function order()
	{
		return $this->belongsTo(\App\Models\Order::class, 'OrderID');
	}
}
