<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 20 Aug 2017 03:28:15 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ProductDetail
 * 
 * @property int $Product_detailID
 * @property int $ProductID
 * @property string $Information
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Product $product
 *
 * @package App\Models
 */
class ProductDetail extends Eloquent
{
	protected $table = 'Product_Detail';
	protected $primaryKey = 'Product_detailID';

	protected $casts = [
		'ProductID' => 'int'
	];

	protected $fillable = [
		'ProductID',
		'Information'
	];

	public function product()
	{
		return $this->belongsTo(\App\Models\Product::class, 'ProductID');
	}
}
