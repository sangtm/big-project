<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 20 Aug 2017 03:28:15 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Payment
 * 
 * @property int $PaymentID
 * @property string $Type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $orders
 *
 * @package App\Models
 */
class Payment extends Eloquent
{
	protected $table = 'Payment';
	protected $primaryKey = 'PaymentID';

	protected $fillable = [
		'Type'
	];

	public function orders()
	{
		return $this->hasMany(\App\Models\Order::class, 'PaymentID');
	}
}
