<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 20 Aug 2017 03:28:15 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Category
 * 
 * @property int $CategoryID
 * @property string $Category_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $products
 *
 * @package App\Models
 */
class Category extends Eloquent
{
	protected $table = 'Category';
	protected $primaryKey = 'CategoryID';

	protected $fillable = [
		'Category_name'
	];

	public function products()
	{
		return $this->hasMany(\App\Models\Product::class, 'CategoryID');
	}
}
