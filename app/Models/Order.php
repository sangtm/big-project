<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 20 Aug 2017 03:28:15 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Order
 * 
 * @property int $OrderID
 * @property int $CustomerID
 * @property int $PaymentID
 * @property \Carbon\Carbon $Payment_Date
 * @property float $Fee
 * @property string $Order_status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Payment $payment
 * @property \App\Models\Customer $customer
 * @property \Illuminate\Database\Eloquent\Collection $order_details
 *
 * @package App\Models
 */
class Order extends Eloquent
{
	protected $primaryKey = 'OrderID';

	protected $casts = [
		'CustomerID' => 'int',
		'PaymentID' => 'int',
		'Fee' => 'float'
	];

	protected $dates = [
		'Payment_Date'
	];

	protected $fillable = [
		'CustomerID',
		'PaymentID',
		'Payment_Date',
		'Fee',
		'Order_status'
	];

	public function payment()
	{
		return $this->belongsTo(\App\Models\Payment::class, 'PaymentID');
	}

	public function customer()
	{
		return $this->belongsTo(\App\Models\Customer::class, 'CustomerID');
	}

	public function order_details()
	{
		return $this->hasMany(\App\Models\OrderDetail::class, 'OrderID');
	}
}
