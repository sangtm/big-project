<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 20 Aug 2017 03:28:15 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Image
 * 
 * @property int $ImageID
 * @property boolean $Img
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $products
 *
 * @package App\Models
 */
class Image extends Eloquent
{
	protected $table = 'Image';
	protected $primaryKey = 'ImageID';

	protected $casts = [
		'Img' => 'boolean'
	];

	protected $fillable = [
		'Img'
	];

	public function products()
	{
		return $this->hasMany(\App\Models\Product::class, 'ImageID');
	}
}
