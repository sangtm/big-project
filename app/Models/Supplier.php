<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 20 Aug 2017 03:28:15 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Supplier
 * 
 * @property int $SupplierID
 * @property int $FacebookID
 * @property string $Name
 * @property string $Phone
 * @property string $Payment_method
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $products
 * @property \Illuminate\Database\Eloquent\Collection $purchases
 *
 * @package App\Models
 */
class Supplier extends Eloquent
{
	protected $table = 'Supplier';
	protected $primaryKey = 'SupplierID';

	protected $casts = [
		'FacebookID' => 'int'
	];

	protected $fillable = [
		'FacebookID',
		'Name',
		'Phone',
		'Payment_method'
	];

	public function products()
	{
		return $this->hasMany(\App\Models\Product::class, 'SupplierID');
	}

	public function purchases()
	{
		return $this->hasMany(\App\Models\Purchase::class, 'SupplierID');
	}
}
