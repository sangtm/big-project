<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 20 Aug 2017 03:28:15 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Product
 * 
 * @property int $ProductID
 * @property string $Description
 * @property int $Price
 * @property int $AdvertisID
 * @property int $CategoryID
 * @property int $ImageID
 * @property int $StatusID
 * @property int $SupplierID
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Status $status
 * @property \App\Models\Category $category
 * @property \App\Models\Image $image
 * @property \App\Models\Advertisment $advertisment
 * @property \App\Models\Supplier $supplier
 * @property \Illuminate\Database\Eloquent\Collection $order_details
 * @property \Illuminate\Database\Eloquent\Collection $product__details
 *
 * @package App\Models
 */
class Product extends Eloquent
{
	protected $table = 'Product';
	protected $primaryKey = 'ProductID';

	protected $casts = [
		'Price' => 'int',
		'AdvertisID' => 'int',
		'CategoryID' => 'int',
		'ImageID' => 'int',
		'StatusID' => 'int',
		'SupplierID' => 'int'
	];

	protected $fillable = [
		'Description',
		'Price',
		'AdvertisID',
		'CategoryID',
		'ImageID',
		'StatusID',
		'SupplierID'
	];

	public function status()
	{
		return $this->belongsTo(\App\Models\Status::class, 'StatusID');
	}

	public function category()
	{
		return $this->belongsTo(\App\Models\Category::class, 'CategoryID');
	}

	public function image()
	{
		return $this->belongsTo(\App\Models\Image::class, 'ImageID');
	}

	public function advertisment()
	{
		return $this->belongsTo(\App\Models\Advertisment::class, 'AdvertisID');
	}

	public function supplier()
	{
		return $this->belongsTo(\App\Models\Supplier::class, 'SupplierID');
	}

	public function order_details()
	{
		return $this->hasMany(\App\Models\OrderDetail::class, 'ProductID');
	}

	public function product__details()
	{
		return $this->hasMany(\App\Models\ProductDetail::class, 'ProductID');
	}
}
