<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 20 Aug 2017 03:28:15 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Customer
 * 
 * @property int $CustomerID
 * @property int $FacebookID
 * @property string $Name
 * @property string $Email
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $orders
 *
 * @package App\Models
 */
class Customer extends Eloquent
{
	protected $table = 'Customer';
	protected $primaryKey = 'CustomerID';

	protected $casts = [
		'FacebookID' => 'int'
	];

	protected $fillable = [
		'FacebookID',
		'Name',
		'Email'
	];

	public function orders()
	{
		return $this->hasMany(\App\Models\Order::class, 'CustomerID');
	}
}
