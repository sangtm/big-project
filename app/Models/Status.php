<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 20 Aug 2017 03:28:15 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Status
 * 
 * @property int $StatusID
 * @property string $Status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $products
 *
 * @package App\Models
 */
class Status extends Eloquent
{
	protected $table = 'Status';
	protected $primaryKey = 'StatusID';

	protected $fillable = [
		'Status'
	];

	public function products()
	{
		return $this->hasMany(\App\Models\Product::class, 'StatusID');
	}
}
